#

## How to compile tarballs

```log
# compile libint 2.7.0+ from tarball - cmake
#cd ${LIBINT2_ROOT} &&
#    tar -xvf ${TARBALL_NAME} &&
#    cd libint-${LIBINT2_VER} &&
#    mkdir -p build && cd build &&
#    CFLAGS='-fPIC' CXXFLAGS="-std=c++11" cmake .. -DREQUIRE_CXX_API=ON -DLIBINT2_BUILD_SHARED_AND_STATIC_LIBS=ON -DCMAKE_INSTALL_PREFIX=${LIBINT2_ROOT}

# compile libint 2.6.0 from tarball - configure supports only in-place builds :(
#cd ${LIBINT2_ROOT} &&
#    tar -xvf ${TARBALL_NAME} &&
#    cd libint-${LIBINT2_VER} &&
#    CFLAGS='-fPIC' CXXFLAGS="-std=c++11" ./configure --enable-shared --prefix=${LIBINT2_ROOT}
```

## 2.9.0 notes

### libint-2.9.0_7_4_7_4.tgz

was generated with, from https://github.com/PhillCli/libint/tree/v2.9.x (a patch adding back missing INCLUDE_DIRECTORIES in INTERFACE CXX target)

```log
../configure --enable-fma --enable-flop-counter --enable-contracted-ints --enable-shared=yes --with-max-am=7 --with-opt-am=4 --with-eri-max-am=7 --with-eri-opt-am=4 --with-1body-max-am=7 --with-1body-opt-am=4 --with-multipole-max-order=4 --with-eri3-max-am=7 --with-eri3-opt-am=4 --with-eri-max-am=7 --with-eri2-opt-am=4
make export
```

### libint-2.9.0_7_4_9_4.tgz

was generated with, from v2.9.0 tag

```log
../configure --enable-fma --enable-flop-counter --enable-contracted-ints --enable-shared=yes --with-max-am=7 --with-opt-am=4 --with-eri-max-am=7 --with-eri-opt-am=4 --with-1body-max-am=7 --with-1body-opt-am=4 --with-multipole-max-order=4 --with-eri3-max-am=9 --with-eri3-opt-am=4 --with-eri-max-am=9 --with-eri2-opt-am=4
make export
```

### libint-2.9.0_7_6_12_6.tgz

was generated with, from v2.9.0 tag
```log
../configure --enable-unrolling=S --enable-fma --enable-flop-counter --enable-contracted-ints --enable-shared=yes --with-max-am=7 --with-opt-am=6 --with-eri-max-am=7 --with-eri-opt-am=6 --with-1body-max-am=7 --with-1body-opt-am=6 --with-multipole-max-order=4 --with-eri3-max-am=12 --with-eri3-opt-am=6 --with-eri-max-am=12 --with-eri2-opt-am=6
make export
```

## Psi4 fork of Libint2 notes, cmake improvments (and more) by Lori
based off https://github.com/evaleev/libint/pull/259
seems libint2 maitanance is slowing down, due to time constrains, thus to have libint2 cmake configs fixed, we can base
on the work that's Lori has already put in.

here's a git diff of options, wrt to that branch, I've used for export of the .tar.gz in this repository; I've tweaked version to be 2.7.3-post1 to not
inflict any conflicts

```git
index 0f13ae17..fa260895 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -9,7 +9,7 @@ endif()
 
 project(
   Libint2Compiler
-  VERSION 2.7.2
+  VERSION 2.7.3
   DESCRIPTION "High-performance library for computing Gaussian integrals in quantum mechanics"
   HOMEPAGE_URL "https://github.com/evaleev/libint"
   LANGUAGES CXX
@@ -87,7 +87,7 @@ option_with_default(LIBINT2_ENABLE_FORTRAN "Build Fortran03+ Libint interface (r
 option_with_default(LIBINT2_ENABLE_PYTHON "Build Python bindings (requires Python and Pybind11 and Eigen3)" OFF)
 option_with_default(LIBINT2_PREFIX_PYTHON_INSTALL "For LIBINT2_ENABLE_PYTHON=ON, whether to install the Python module in the Linux manner to CMAKE_INSTALL_PREFIX or to not install it. See target libint2-python-wheel for alternate installation in the Python manner to Python_EXECUTABLE's site-packages." OFF)
 
-option_with_print(BUILD_SHARED_LIBS "Build Libint library as shared, not static" OFF)
+option_with_print(BUILD_SHARED_LIBS "Build Libint library as shared, not static" ON)
 option_with_print(LIBINT2_BUILD_SHARED_AND_STATIC_LIBS "Build both shared and static Libint libraries in one shot. Uses -fPIC." OFF)
 
 #  <<<  Which Integrals Classes, Which Derivative Levels  >>>
@@ -97,9 +97,9 @@ option_with_default(ENABLE_ONEBODY
 option_with_default(ENABLE_ERI
   "Compile with support for up to N-th derivatives of 4-center electron repulsion integrals (-1 for OFF)" 0)
 option_with_default(ENABLE_ERI3
-  "Compile with support for up to N-th derivatives of 3-center electron repulsion integrals (-1 for OFF)" -1)
+  "Compile with support for up to N-th derivatives of 3-center electron repulsion integrals (-1 for OFF)" 0)
 option_with_default(ENABLE_ERI2
-  "Compile with support for up to N-th derivatives of 2-center electron repulsion integrals (-1 for OFF)" -1)
+  "Compile with support for up to N-th derivatives of 2-center electron repulsion integrals (-1 for OFF)" 0)
 option_with_default(ENABLE_G12
   "Compile with support for N-th derivatives of MP2-F12 energies with Gaussian factors (-1 for OFF)" -1)
 option_with_default(ENABLE_G12DKH
@@ -109,7 +109,7 @@ option_with_print(DISABLE_ONEBODY_PROPERTY_DERIVS
   "Disable geometric derivatives of 1-body property integrals (all but overlap, kinetic, elecpot).
    These derivatives are disabled by default to save compile time. (enable with OFF)" ON)
 option_with_print(ENABLE_T1G12_SUPPORT
-  "Enable Ti,G12 integrals when G12 integrals are enabled. Irrelevant when `ENABLE_G12=OFF`. (disable with OFF)" ON)
+       "Enable Ti,G12 integrals when G12 integrals are enabled. Irrelevant when `ENABLE_G12=OFF`. (disable with OFF)" OFF)
 
 #  <<<  Ordering Conventions  >>>
 
@@ -157,7 +157,7 @@ option_with_print(ERI2_PURE_SH
 
 option_with_default(WITH_MAX_AM
   "Support Gaussians of angular momentum up to N.
-   Can specify values for each derivative level as a semicolon-separated string" 4)
+   Can specify values for each derivative level as a semicolon-separated string" 7)
 option_with_default(WITH_OPT_AM
   "Optimize maximally for up to angular momentum N (N <= max-am).
    Can specify values for each derivative level as a semicolon-separated string. (default: (libint_max_am/2)+1)" -1)
```
